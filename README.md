## Description

CRUD Example proyect in Nest.js by Jimena Danieli



## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

```

## Docker - Compose

```bash
# Build Image and Run
docker-compose up -d
```

## Docker - Legacy

```bash
# Build Image
docker-compose up -d
docker build -t nest_crud_jimena .

# Run image
docker run -p 3000:3000 nest_crud_jimena
```

## Swagger

```bash
# Documentation
http://localhost:3000/api
```
