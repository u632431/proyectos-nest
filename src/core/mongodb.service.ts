import { Injectable } from '@nestjs/common';
import { MongoClient, Db, Collection } from 'mongodb';

@Injectable()
export class MongoDBService {
  private client: MongoClient;
  private db: Db;

  constructor() {
    this.connect();
  }

  private async connect() {
    try {
      this.client = await MongoClient.connect(process.env.MONGO_URL);
      this.db = this.client.db(process.env.MONGO_DB);
      console.log(
        'Time:',
        new Date().toLocaleString(),
        'MongoDB:',
        'Connected to MongoDB')
    } catch (error) {
      console.error('Error connecting to MongoDB:', error);
    }
  }

  public async insertOne(collectionName: string, document: any) {
    const collection: Collection = this.db.collection(collectionName);
    const result = await collection.insertOne(document);
    return result.insertedId;
  }

  public async findOne(collectionName: string, query: any) {
    const collection: Collection = this.db.collection(collectionName);
    return collection.findOne(query);
  }

  public async findMany(collectionName: string, query: any) {
    const collection: Collection = this.db.collection(collectionName);
    const cursor = collection.find(query);
    const documents = await cursor.toArray();
    return documents;
  }

  public async findManyProject(
    collectionName: string,
    query: any,
    projection: any,
  ) {
    const collection: Collection = this.db.collection(collectionName);
    const cursor = collection.find(query).project(projection);
    const documents = await cursor.toArray();
    return documents;
  }

  public async updateOne(collectionName: string, query: any, update: any) {
    const collection: Collection = this.db.collection(collectionName);
    const result = await collection.updateOne(query, { $set: update });
    return result.modifiedCount;
  }

  public async deleteOne(collectionName: string, query: any) {
    const collection: Collection = this.db.collection(collectionName);
    const result = await collection.deleteOne(query);
    return result.deletedCount;
  }

  public async deleteMany(collectionName: string, query: any) {
    const collection: Collection = this.db.collection(collectionName);
    const result = await collection.deleteMany(query);
    return result.deletedCount;
  }
}
