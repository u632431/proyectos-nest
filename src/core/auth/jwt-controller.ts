import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserRepository } from 'src/domain/app-crud/repositorys/user.repository';
import { UserUnauthorized } from 'src/domain/app-crud/exceptions/custom.exceptions';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  // Inyectamos el servicio de autenticación y el repositorio de usuarios
  constructor(private readonly authService: AuthService, private readonly userRepository : UserRepository) {}

  // Metodo login que recibe un usuario y una contraseña y devuelve un token
  @ApiOperation({ summary: 'Login - Obtener Token' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Post('login')
  async login(@Body() user: { username: string, password: string }): Promise<{ access_token: string }> {

    // Vamos al repositorio contra el metodo findUserByEmailPassword (esto impacta contra mongo), le pasamos el usuario y la contraseña para validar
    const validUser = await  this.userRepository.findUserByEmailPassword(user.username, user.password);
    
    // Si el usuario no existe, lanza una excepción, de lo contrario genera un token y lo devuelve en el response
    if (validUser) {
      // Genera el token en el servicio de autenticación
      const token = await this.authService.generateToken({ sub: user.username });
      return { access_token: token };
    } else {
      throw new UserUnauthorized();
    }
  }
}