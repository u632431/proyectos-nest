import { diskStorage } from 'multer';
import { extname } from 'path';

export const multerOptions = {
  //guardamos todo en la api
  storage: diskStorage({
    destination: 'uploads_images', // Directorio donde se almacenan los archivos
    filename: (req, file, cb) => {
      //genera un hash filtrando los archivos ya generados. Me la posibilidad de guardar la misma imagen (con el mismo nombre) pero se toman distintos hash
      const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
      //este cb, lo renombra y "es algo nuevo"
      cb(null, `${randomName}${extname(file.originalname)}`);
    },
  }),
};