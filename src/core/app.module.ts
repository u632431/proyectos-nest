import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { LoggerMiddleware } from './logger.middleware';
import { UserService } from '../domain/app-crud/services/user.service';
import { UserController } from '../domain/app-crud/controllers/user.controller';
import { UserRepository } from 'src/domain/app-crud/repositorys/user.repository';
import { MongoDBService } from './mongodb.service';
import { ProductController } from 'src/domain/app-crud/controllers/product.controller';
import { ProductRepository } from 'src/domain/app-crud/repositorys/product.repository';
import { ProductService } from 'src/domain/app-crud/services/product.service';
import { webSocketGateway } from '../domain/app-crud/websocket/websocket.gateway';
import { AuthService } from './auth/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth/jwt-controller';
import { JwtStrategy } from './auth/jwt.strategy';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';

@Module({
  imports: [JwtModule.register({
    secret: process.env.JWT_SECRET, 
    signOptions: { expiresIn: process.env.JWT_EXPIRES_TIME },
  }),
  ServeStaticModule.forRoot({
    serveRoot: '/uploads_images',
    rootPath: join(__dirname, 'uploads_images'), // Directorio donde se encuentran las imágenes
  }),

],
  controllers: [UserController, ProductController, AuthController],
  providers: [
    MongoDBService,
    UserService,
    ProductService,
    UserRepository,
    ProductRepository,
    webSocketGateway,
    AuthService,
    JwtStrategy
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');


  }
}
