import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  //Logger:  Puede controlar completamente el comportamiento del sistema de registro. En lugar de un clg, puedo implementar otras librerias
  use(req: Request, res: Response, next: NextFunction) {
    console.log(
      'Time:',
      new Date().toLocaleString(),
      'req:',
      req.baseUrl,
      'statusCode:',
      res.statusCode,
    );
    next();
  }
}
