export class ProductDto {
    readonly _id: string;
    readonly name: string;
    readonly size: string[];
    readonly category: string;
    readonly price: number;
    readonly image: string;
    readonly active: boolean;
  }