export class UserDto {
    readonly _id: string;
    readonly name: string;
    readonly lastname: string;
    readonly email: string;
    readonly pass?: string;
    readonly age: number;
    readonly role: string;
    readonly active: boolean;
  }