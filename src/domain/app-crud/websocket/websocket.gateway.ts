// websocket/websocket.gateway.ts
import {SubscribeMessage, WebSocketGateway, WebSocketServer,} from '@nestjs/websockets';
import { Server } from 'socket.io';
import { ProductService } from 'src/domain/app-crud/services/product.service';

@WebSocketGateway({ cors: true })
export class webSocketGateway {
  @WebSocketServer() server: Server;
  constructor(private readonly productService: ProductService) {}

  @SubscribeMessage('message')
  async handleMessage(client: any, payload: any): Promise<void> {
    console.log(
      'Time:',
      new Date().toLocaleString(),
      'WSK:',
      client.id,
      'payload:',
      payload,
    );
    const products = await this.productService.getPorductRegex(payload);
    if (!products || payload.length === 0) {
      this.server.emit('message', 'Product not found');
    } else {
      this.server.emit('message', products['name']);
    }
  }
}
