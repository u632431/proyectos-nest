import {Body, Controller, Delete, Get, Param, Patch, Post, Res, UploadedFile, UseInterceptors,} from '@nestjs/common';
import { ProductService } from '../services/product.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { multerOptions } from '../../../core/multer.config';
import { FileInterceptor } from '@nestjs/platform-express';
import { join } from 'path';
import { Response } from 'express';
import { createReadStream } from 'fs';

@ApiTags('Products')
@Controller()
export class ProductController {
  constructor(private readonly productService: ProductService) {}
  //metodos propios de swagger
  @ApiOperation({ summary: 'Obtener todos los productos' })
  @ApiResponse({ status: 200, description: 'OK' })

  //aca empieza la peticion propiamente dicha
  @Get('products')
  async getProducts(): Promise<any> {
    return this.productService.getProducts();
  }

  @ApiOperation({ summary: 'Crear productos' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Post('products')
  async createProduct(@Body() product: any): Promise<any> {
    return this.productService.createProduct(product);
  }

  @ApiOperation({ summary: 'Crear Imagen' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Post('products/image')
  //Usamos un interceptor de tipo file, delegamos la responsabilidad a multerOptions
  @UseInterceptors(FileInterceptor('image', multerOptions))
  async createProductImage(@UploadedFile() image: Express.Multer.File): Promise<any> {
    const FileObj = {"file_name":image.filename};
    //este servicio cambia el nombre del archivo, va a mongo a guardarlo
    await this.productService.createProductImage(FileObj);
    //devuelvo la respuesta del endoipt que me da una url con el nombre del archivo
    const response = {"url_file_uploaded": process.env.PATH_URL_IMAGES + image.filename}
    return response ;
  }

  @ApiOperation({ summary: 'Actualizar productos' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Patch('products/:id')
  async updateProduct(
    @Param('id') id: string,
    @Body() product: any,
  ): Promise<any> {
    return this.productService.updateProduct(id, product);
  }

  // Use soft delete instead of hard delete
  @ApiOperation({ summary: 'Eliminar productos' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Delete('products/:id')
  async deleteProduct(@Param('id') id: string): Promise<any> {
    return this.productService.deleteProduct(id);
  }

  @ApiOperation({ summary: 'Obtener imagenes productos' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Get('image/:filename')
  getProductImage(@Param('filename') filename: string, @Res() res: Response) {
    const file = createReadStream(join(process.cwd(), 'uploads_images', filename));
    file.pipe(res);
  }

}
