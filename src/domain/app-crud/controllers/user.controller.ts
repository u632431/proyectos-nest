import {Body, Controller, Delete, Get, Param, Patch, Post, UseGuards,} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { ApiResponse, ApiTags, ApiOperation } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/core/auth/jwt-auth.guard';

@ApiTags('Users')
@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Obtener usuarios - Requiere TOKEN' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Get('users')
  //Todo lo que llegue a la petición, primero pasa por el guard
  @UseGuards(JwtAuthGuard) //esto espera un true para seguir (se lo damos en el strategy). Tiene que ir debajo de mi verbo
  async getUsers(): Promise<any> {
    return this.userService.getUsers();
  }

  @ApiOperation({ summary: 'Crear usuarios' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Post('users')
  async createUser(@Body() user: any): Promise<any> {
    return this.userService.createUser(user);
  }

  @ApiOperation({ summary: 'Actualizar usuarios' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Patch('users/:id')
  async updateUser(@Param('id') id: string, @Body() user: any): Promise<any> {
    return this.userService.updateUser(user, id);
  }

  // Use soft delete instead of hard delete para mantener la traza de la info. Es la forma de saber que hay y qué había. Es reversible
  @ApiOperation({ summary: 'Eliminar usuarios' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Delete('users/:id')
  async deleteUser(@Param('id') id: string): Promise<any> {
    return this.userService.deleteUser(id);
  }
}
