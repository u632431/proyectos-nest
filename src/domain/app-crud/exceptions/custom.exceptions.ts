import { HttpException, HttpStatus } from '@nestjs/common';

export class ProductAlreadyExistsException extends HttpException {
  constructor() {
    super('Product already exists', HttpStatus.CONFLICT);
  }
}

export class UserAlreadyExistsException extends HttpException {
  constructor() {
    super('User already exists', HttpStatus.CONFLICT);
  }
}

export class UserUnauthorized extends HttpException {
  constructor() {
    super('User not Authorized', HttpStatus.UNAUTHORIZED);
  }
}