import { Injectable } from '@nestjs/common';
import { MongoDBService } from '../../../core/mongodb.service';
import { ObjectId } from 'mongodb';
import { UserDto } from '../dtos/user.dto';
import { UserAlreadyExistsException } from '../exceptions/custom.exceptions';

@Injectable()
export class UserRepository {
  constructor(private readonly mongoSrvs: MongoDBService) {}

  async findUsers(): Promise<UserDto[]> {
    const users = await this.mongoSrvs.findManyProject(
      'users',
      { active: true },
      { pass: 0 },
    );

    const userDtos = users.map((user) => ({
      _id: user._id,
      name: user.name,
      lastname: user.lastname,
      email: user.email,
      age: user.age,
      role: user.role,
      active: user.active,
    }));
    return userDtos;
  }

  async createUser(user): Promise<any> {
    const userExist = await this.mongoSrvs.findOne('users', {
      email: user.email,
    });
    if (userExist) {
      throw new UserAlreadyExistsException();
    }
    return await this.mongoSrvs.insertOne('users', user);
  }

  async updateUser(user, id): Promise<any> {
    return await this.mongoSrvs.updateOne(
      'users',
      { _id: new ObjectId(id) },
      user,
    );
  }

  // Use soft delete instead of hard delete
  async deleteUser(id): Promise<any> {
    return await this.mongoSrvs.updateOne(
      'users',
      { _id: new ObjectId(id) },
      { active: false },
    );
  }

  async findUserByEmailPassword(email, pass): Promise<any> {
    return await this.mongoSrvs.findOne('users', { email: email, pass: pass });
  }
}
