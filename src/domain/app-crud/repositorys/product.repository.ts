import { Injectable } from '@nestjs/common';
import { MongoDBService } from '../../../core/mongodb.service';
import { ObjectId } from 'mongodb';
import { ProductDto } from '../dtos/product.dto';
import { ProductAlreadyExistsException } from '../exceptions/custom.exceptions';

@Injectable()
export class ProductRepository {
  constructor(private readonly mongoSrvs: MongoDBService) {}

  async findProducts(): Promise<ProductDto[]> {
    const products = await this.mongoSrvs.findMany('products', {
      active: true,
    });
    const productDtos = products.map((product) => ({
      _id: (product._id as ObjectId).toHexString(),
      name: product.name,
      size: product.size,
      category: product.category,
      price: product.price,
      image: product.image,
      active: product.active,
    }));
    return productDtos;
  }

  async createProduct(product): Promise<any> {
    const productExist = await this.mongoSrvs.findOne('products', {
      name: product.name, active : true,
    });
    if (productExist) {
      throw new ProductAlreadyExistsException();
    }
    return await this.mongoSrvs.insertOne('products', product);
  }

  async createProductImage(FileName): Promise<any> {
    return await this.mongoSrvs.insertOne('images', FileName);
  }

  async updateProduct(id, product): Promise<any> {
    return await this.mongoSrvs.updateOne(
      'products',
      { _id: new ObjectId(id) },
      product,
    );
  }

  // Use soft delete instead of hard delete
  async deleteProduct(id): Promise<any> {
    return await this.mongoSrvs.updateOne(
      'products',
      { _id: new ObjectId(id) },
      { active: false },
    );
  }

  async findProductRegex(name): Promise<any> {
    return await this.mongoSrvs.findOne('products', {
      name: { $regex: name, $options: 'i' },
    });
  }
}
