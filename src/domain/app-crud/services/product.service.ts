import { Injectable } from '@nestjs/common';
import { ProductRepository } from '../repositorys/product.repository';

@Injectable()
export class ProductService {
  constructor(private readonly productRepository: ProductRepository) {}

  async getProducts(): Promise<any> {
    return this.productRepository.findProducts();
  }

  //Operador de mongo, busca "algo similar"
  async getPorductRegex(name): Promise<any> {
    return this.productRepository.findProductRegex(name);
  }

  async createProduct(product): Promise<any> {
    return this.productRepository.createProduct(product);
  }

  async createProductImage(FileName): Promise<any> {
    return this.productRepository.createProductImage(FileName);
  }

  async updateProduct(id, product): Promise<any> {
    return this.productRepository.updateProduct(id, product);
  }
  
  async deleteProduct(id): Promise<any> {
    return this.productRepository.deleteProduct(id);
  }
}
