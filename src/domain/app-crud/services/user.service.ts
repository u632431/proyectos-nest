import { Injectable } from '@nestjs/common';
import { UserRepository } from '../repositorys/user.repository';


@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository ) {}

  async getUsers(): Promise<any> {
    return this.userRepository.findUsers();
  }

  async createUser(user): Promise<any> {
    return this.userRepository.createUser(user);
  }

  async updateUser(user, id): Promise<any> {
    return this.userRepository.updateUser(user, id);
  }
  
  async deleteUser(id): Promise<any> {
    return this.userRepository.deleteUser(id);
  }
}
